﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StepManiaSongOrganizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            var baseDir = @"C:\Users\crwilcox\Desktop\StepMania 5\Songs";

            var letterDirectories = Directory.GetDirectories(baseDir, "*", SearchOption.TopDirectoryOnly);

            foreach (var letterDirectory in letterDirectories)
            {
                Console.Write(letterDirectory + " : ");
                var count = 0;
                var songDirectories = Directory.GetDirectories(letterDirectory, "*", SearchOption.TopDirectoryOnly);
                foreach (var songDirectory in songDirectories)
                {
                    count++;
                    Console.Write("\r" + letterDirectory + " : " + count);

                    p.RenameFolderToMatchFile(baseDir, songDirectory);
                }
                Console.WriteLine();
            }

            Console.WriteLine("Complete... Press any key to exit...");
            Console.ReadKey();
        }

        /// <summary>
        /// Given a path, go and find the sm, dwi, or bms file (in that order)
        /// Replace the folder name with the title - subtitle - artist, for ease of viewing
        /// on collision, add - (#) to this
        /// </summary>
        /// <param name="songDirectory">the path to the song directory</param>
        private void RenameFolderToMatchFile(string baseDir, string songDirectory)
        {
            string title = String.Empty;
            string subtitle = String.Empty;
            string artist = String.Empty;

            bool useSmFile = false;
            bool useDwiFile = false;
            bool useBmeFile = false;
            bool useSscFile = false;

            // find the beats file
            // look for sm file
            var smFiles = Directory.GetFiles(songDirectory, "*.sm", SearchOption.TopDirectoryOnly);
            // look for dwi file
            var dwiFiles = Directory.GetFiles(songDirectory, "*.dwi", SearchOption.TopDirectoryOnly);
            // look for bms file
            var bmeFiles = Directory.GetFiles(songDirectory, "*.bme", SearchOption.TopDirectoryOnly);

            var sscFiles = Directory.GetFiles(songDirectory, "*.ssc", SearchOption.TopDirectoryOnly);


            string pathToBeatsFile = string.Empty;

            if (smFiles.Length != 0)
            {
                pathToBeatsFile = smFiles.First();
                useSmFile = true;
            }
            else if (dwiFiles.Length != 0)
            {
                pathToBeatsFile = dwiFiles.First();
                useDwiFile = true;
            }
            else if (bmeFiles.Length != 0)
            {
                pathToBeatsFile = bmeFiles.First();
                useBmeFile = true;
            }
            else if (sscFiles.Length != 0)
            {
                pathToBeatsFile = sscFiles.First();
                useSscFile = true;
            }
            else
            {
                // could throw here, but instead, let's move them to a common spot for later investigation...
                // throw new NotSupportedException("We don't have a type for the beat file you are trying to look at...");
                if(!Directory.Exists(Path.Combine(baseDir,"#UNKNOWN_BEAT_FILE")))
                {
                    Directory.CreateDirectory(Path.Combine(baseDir, "#UNKNOWN_BEAT_FILE"));
                }
                var destination = Path.Combine(baseDir, "#UNKNOWN_BEAT_FILE", Path.GetFileName(songDirectory));
                if (songDirectory != destination)
                {
                    Directory.Move(songDirectory, destination);
                }
            }

            if (!String.IsNullOrWhiteSpace(pathToBeatsFile))
            {
                GetSongInformationFromBeatsFile(ref title, ref subtitle, ref artist, useBmeFile, pathToBeatsFile);

                string letter = FindSubFolderUsingTitle(title);

                // once we have the folder name, do a search to make sure others don't exist.  if they do, add a number at the end to patch it up...
                var newSongDirectory = BuildSongDirectory(title, subtitle, artist);

                var destinationPath = Path.Combine(baseDir, letter, newSongDirectory);

                // We can only attempt a move if the destination and source are different.  Also, we don't want to attempt a move
                // if they are already named properly with a number after...
                if (!songDirectory.Equals(destinationPath) && !songDirectory.StartsWith(destinationPath))
                {
                    destinationPath = GetUniquePath(destinationPath);

                    if (Directory.Exists(destinationPath))
                    {
                        throw new NotSupportedException("We have a collision for the destination path");
                    }
                    else
                    {
                        Directory.Move(songDirectory, destinationPath);
                    }
                }
            }
        }

        private static string BuildSongDirectory(string title, string subtitle, string artist)
        {
            var newSongDirectory = title + "-" + subtitle + "-" + artist;
            newSongDirectory = MakeValidFileName(newSongDirectory);
            return newSongDirectory;
        }

        private static string FindSubFolderUsingTitle(string title)
        {
            // find what folder letter this should go in...
            string letter = "0-9";
            if (title.Length > 0)
            {
                var firstChar = title.ToUpper()[0];
                if (firstChar >= 'A' && firstChar <= 'Z')
                {
                    letter = firstChar.ToString();
                }
            }
            return letter;
        }

        private static void GetSongInformationFromBeatsFile(ref string title, ref string subtitle, ref string artist, bool useBmeFile, string pathToBeatsFile)
        {
            //crack open the file, and get the title, subtitle, and artist from it...
            var beatsFileLines = File.ReadAllLines(pathToBeatsFile);
            foreach (var line in beatsFileLines)
            {
                if (useBmeFile)
                {
                    var lowerLine = line.ToLower();

                    if (lowerLine.StartsWith("#title "))
                    {
                        title = line.Substring(("#title ").Length);
                    }
                    else if (lowerLine.StartsWith("#subtitle "))
                    {
                        subtitle = line.Substring(("#subtitle ").Length);
                    }
                    else if (lowerLine.StartsWith("#artist "))
                    {
                        artist = line.Substring(("#artist ").Length);
                    }
                }
                else
                {
                    var lowerLine = line.ToLower();

                    if (lowerLine.StartsWith("#title:"))
                    {
                        var start = line.IndexOf(':') + 1;
                        var end = line.IndexOf(';');
                        if (end == -1)
                        {
                            title = line.Substring(start);
                        }
                        else
                        {
                            title = line.Substring(start, end - start);
                        }
                    }
                    else if (lowerLine.StartsWith("#subtitle:"))
                    {
                        var start = line.IndexOf(':') + 1;
                        var end = line.IndexOf(';');
                        if (end == -1)
                        {
                            subtitle = line.Substring(start);
                        }
                        else
                        {
                            subtitle = line.Substring(start, end - start);
                        }
                    }
                    else if (lowerLine.StartsWith("#artist:"))
                    {
                        var start = line.IndexOf(':') + 1;
                        var end = line.IndexOf(';');
                        if (end == -1)
                        {
                            artist = line.Substring(start);
                        }
                        else
                        {
                            artist = line.Substring(start, end - start);
                        }
                    }
                }
            }
        }

        private static string GetUniquePath(string path)
        {
            // if the directory isn't found, we just return path
            var outputPath = path;

            // loop until we generate a unique path
            var count = 1;
            while (Directory.Exists(outputPath))
            {
                count++;
                outputPath = String.Format("{0}-({1})", path, count);
            }

            return outputPath;
        }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            return Regex.Replace(name, invalidReStr, "_");
        }
    }
}
