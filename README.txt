Started On: October 22, 2012

I have quite the collection of StepMania songs, but the issue has been 
organization of these songs.  I wrote a tool that given a directory, will 
refolder them by title, subtitle, and artist.  This makes it easier to find 
duplicates, and I personally like navigating by the first letter of the title